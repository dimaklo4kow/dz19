<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return redirect('/product');
});


Route::get('/product', 'ProductsController@index')->name('home');
Route::get('/pages', 'PagesController@index');
Route::get('/orders', 'OrdersController@index');
Route::get('/product/{product}', 'ProductsController@show');
Route::get('/orders/add', 'OrdersController@add');
Route::post('/orders/store', 'OrdersController@store');
Route::get('/products/add', 'ProductsController@add');
Route::post('/products/add', 'ProductsController@store');
Route::get('/pages/create', 'PagesController@create');
Route::post('/pages/create', 'PagesController@store');

Route::get('/login', 'SessionController@create');
Route::post('/login', 'SessionController@store');
Route::get('/logout', 'SessionController@destroy');

Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');