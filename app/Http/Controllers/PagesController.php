<?php

namespace App\Http\Controllers;

use App\Pages;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Pages::all();
        return view('Pages.index', compact('pages'));
    }

    public function create()
    {
        return view('Pages.create');
    }
    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'content' => 'required',
        ]);
        Pages::create([
            'title' => request('title'),
            'alias' => request('alias'),
            'content' => request('content'),
        ]);
        return redirect('/pages');
    }
}
