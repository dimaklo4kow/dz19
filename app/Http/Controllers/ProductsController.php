<?php

namespace App\Http\Controllers;

use App\Products;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Products::All();
        return view('Products.index', compact('products'));
    }

    public function show($id)
    {
        $products = Products::find($id);
        return view('Products.show', compact('products'));
    }

    public function add()
    {
        return view('Products.add');
    }
    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'price' => 'required',
            'description' => 'required',
        ]);

        Products::create([
            'title' => request('title'),
            'alias' => request('alias'),
            'price' => request('price'),
            'description' => request('description'),
        ]);
        return redirect('/product');
    }
}
