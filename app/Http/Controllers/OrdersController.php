<?php

namespace App\Http\Controllers;

use App\Orders;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $orders = Orders::all();
        return view('Orders.index', compact('orders'));
    }

    public function add()
    {
        return view('Orders.add');
    }

    public function store()
    {
        $this->validate(request(), [
            'email' => 'required',
        ]);

        Orders::create([
            'customer_name' => request('customer_name'),
            'email' => request('email'),
            'phone' => request('phone'),
            'feedback' => request('feedback'),
        ]);

        return redirect('/orders');
    }

}
