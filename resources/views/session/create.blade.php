@extends('layout')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <form action="/login" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" id="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" id="password" class="form-control">
            </div>

            <button class="btn btn-success" type="submit">Login</button>
        </form>
    </div>
@endsection