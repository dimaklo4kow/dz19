@extends('layout')

@section('content')
    <div class="col-md-5 col-sm-offset-2">
        <form action="/pages/create" method="post">
            {{csrf_field()}}
            <div>
                <label for="title">Введите заголовок:</label>
                <input type="text" name="title" id="title" class="form-control" required>
            </div>
            <div>
                <label for="alias">Введите алиас:</label>
                <input type="text" name="alias" id="alias" class="form-control" required>
            </div>
            <div>
                <label for="content">Введите контент:</label>
                <input type="text" name="content" id="content" class="form-control" required>
            </div>
            <br>
            <div>
                <input type="submit" class="btn btn-info">
            </div>
        </form>
    </div>
@stop