@extends('layout')
@section('content')
    @foreach($pages AS $page)
        <div class="col-md-4">
            <h2>{{$page->title}}</h2>
            <p>Алиас: {{$page->alias}}</p>
            <p>Контент: {{$page->content}}</p>
            </div>
    @endforeach
    <a href="/pages/create" class="btn btn-success">Create</a>
@stop