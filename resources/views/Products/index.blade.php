@extends('layout')

@section('content')
    <div class="col-md-1 col-lg-1 col-lg-offset-7 col-sm-1 col-sm-offset-7 col-md-offset-7">
        <a class="btn btn-success" href="/products/add"><p>Добавить товар</p></a>
    </div><br><br><br>
    @foreach($products AS $product)
        <div class="col-md-4">
            <h2>{{$product->title}}</h2>
            <p>Цена: {{$product->price}}</p>
            <p>Описание: {{$product->description}}</p>
            <a href="/product/{{$product->id}}" class="btn-info btn">Детальнее</a>
        </div>
    @endforeach
@stop