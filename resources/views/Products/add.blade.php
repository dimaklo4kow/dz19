@extends('layout')

@section('content')
    <div class="col-md-6">
        <form action="" method="post">
            {{csrf_field()}}

            <label for="title">Введите название:</label>
            <input  class="form-control" type="text" name="title" id="title">

            <label for="alias">Латинское название:</label>
            <input type="text" name="alias" id="alias" class="form-control">

            <label for="price">Укажите цену:</label>
            <input type="text" name="price" id="price" class="form-control">

            <label for="description">Добавте коментарий:</label>
            <textarea name="description" id="description" cols="30" rows="5" class="form-control" ></textarea>
            <br>
            <input type="submit" class="btn-info btn">
        </form>

        @if(count($errors))
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() AS $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
@stop