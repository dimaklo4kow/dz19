@extends('layout')

@section('content')
    <div>
        <h3>{{$products->title}}</h3>
        <p>Цена: {{$products->price}}</p>
        <p>Описание: {{$products->description}}</p>
        <a href="/orders/add" class="btn-info btn">Заказать</a>
    </div>
@stop