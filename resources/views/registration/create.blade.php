@extends('layout')

@section('content')
    <div class="col-md-6 col-md-offset-3">
        <h2>Регистрация</h2>
        <form action="/register" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Введите имя</label>
                <input type="text" name="name" id="name" class="form-control">
            </div>

            <div class="form-group">
                <label for="email">Введите email</label>
                <input type="text" name="email" id="email" class="form-control">
            </div>

            <div class="form-group">
                <label for="password">Введите пароль</label>
                <input type="password" name="password" id="password" class="form-control">
            </div>

            <button type="submit" class="btn btn-success">Зарегистрироваться</button>
        </form>
    </div>
@endsection