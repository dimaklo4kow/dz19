@extends('layout')

@section('content')
    <div class="col-md-6">
        <form action="/orders/store" method="post">
            {{csrf_field()}}

            <label for="customer_name">Введите имя:</label>
           <input  class="form-control" type="text" name="customer_name" id="customer_name">

            <label for="email">Введите email:</label>
            <input type="text" name="email" id="email" class="form-control">

            <label for="phone">Введите номер телефона:</label>
            <input type="text" name="phone" id="phone" class="form-control">

            <label for="feedback">Комментарий:</label>
            <textarea name="feedback" id="feedback" cols="30" rows="5" class="form-control" ></textarea>
            <br>
            <input type="submit" class="btn-info btn">
        </form>

        @if(count($errors))
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() AS $error)
                            <li>
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
@stop